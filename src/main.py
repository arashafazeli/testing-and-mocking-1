"""This module fetches data from an API. For the sake of our test, let's assume the API calls are very expensive."""
import json
import time

import requests

BASE_API_URL = "https://api.punkapi.com/v2"


def get_beer_page(page_n):
    """This function retrieves one page of results from the API """
    response = requests.get(f"{BASE_API_URL}/beers?page={page_n}")
    if response.status_code != 200:
        raise RuntimeError(f"Error fetching beers! Got status {response.status_code} {response.reason}")
    return response.json()


def get_all_beers():
    """This function retrieves *all* the beer recipes from PunkAPI."""
    beer_list = []
    page_n = 1
    results = get_beer_page(page_n)
    while results:
        beer_list.extend(results)
        page_n += 1
        # There is a 1 req/sec rate limit on the PunkAPI.
        time.sleep(1)
        print(f"Fetching recipe page {page_n}...", end="\r")
        results = get_beer_page(page_n)
    return beer_list


def main():
    beers = get_all_beers()
    with open("beers.json", "w") as file:
        json.dump(beers, file, indent=4)


if __name__ == "__main__":
    main()
