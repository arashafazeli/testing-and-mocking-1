"""Unit tests for main.py"""

from unittest.mock import patch
from pytest import raises
import os

from main import get_beer_page, get_all_beers, main


def test_get_beer_page_ok():
    beers = get_beer_page(1)
    assert beers != []


class MockResponse:
    reason = "Mock reason"

    def __init__(self, data, status_code):
        self.data = data
        self.status_code = status_code

    def json(self):
        return self.data


def mock_sleep(*args):
    pass


def mock_requests_get(*args):
    if args[0].endswith("1"):
        return MockResponse([{"beer": "recipe"}], 200)
    else:
        return MockResponse([], 200)


def mock_requests_get_err(*args):
    return MockResponse(None, 404)


@patch("requests.get", side_effect=mock_requests_get_err)
def test_get_beer_page_err(mock_get):
    with raises(RuntimeError):
        get_beer_page(1)
    assert mock_get.called


@patch("time.sleep", side_effect=mock_sleep)
@patch("requests.get", side_effect=mock_requests_get)
def test_get_all_beers(mock_get, sleep_mock):
    beers = get_all_beers()
    assert beers == [{"beer": "recipe"}]
    assert sleep_mock.called
    assert mock_get.called


@patch("time.sleep", side_effect=mock_sleep)
@patch("requests.get", side_effect=mock_requests_get)
def test_main(get_mock, sleep_mock, tmp_path):
    os.chdir(tmp_path)
    main()
    p = tmp_path / "beers.json"
    assert p.exists()
    with open(p) as file:
        assert file.read() == """[
    {
        "beer": "recipe"
    }
]"""
    assert get_mock.called
    assert sleep_mock.called
